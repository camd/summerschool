==================
Summer school 2018
==================

.. note::

    All the projects have been moved to GPAW's webpage_.
    Please make merge requests to GPAW's git repository_
    if you want to change something.


.. _webpage: https://wiki.fysik.dtu.dk/gpaw/dev/summerschools/summerschool18/summerschool18.html
.. _repository: https://gitlab.com/gpaw/gpaw/tree/master/doc/summerschools/summerschool18


Links
=====

* `Official web-page
  <http://www.fysik.dtu.dk/english/Research/CAMD/Events/Summer-School-2018>`__
* ASE_
* GPAW_

.. _ASE: https://wiki.fysik.dtu.dk/ase
.. _GPAW: https://wiki.fysik.dtu.dk/gpaw
