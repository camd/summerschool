import json
import sys
from pathlib import Path

path = Path(sys.argv[1])
data = json.loads(path.read_text())

lines = [f'# Converted from {path}\n']
n = 1
for cell in data['cells']:
    if cell['cell_type'] == 'code':
        lines.extend(['\n', f'# In [{n}]:\n'])
        for line in cell['source']:
            if line.startswith('%') or line.startswith('!'):
                line = '# ' + line
            lines.append(line)
        lines.append('\n')
        n += 1

path.with_suffix('.py').write_text(''.join(lines))
