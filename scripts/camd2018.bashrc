
module load python3
#module load gcc/6.4.0
module load scalapack

export PATH=~jasc/CAMD2018/summerschool/scripts:$PATH

export OMPI_MCA_rmaps_base_oversubscribe=1
complete -o default -C "/zhome/86/d/1666/summer/bin/python3 /zhome/86/d/1666/summer/lib/python3.6/site-packages/ase/cli/complete.py" ase
complete -o default -C "/zhome/86/d/1666/summer/bin/python3 /zhome/86/d/1666/summer/lib/python3.6/site-packages/gpaw/cli/complete.py" gpaw
. ~jjmo/summer/bin/activate
alias l="ls -ltr"
alias p=python3
alias e="emacs -nw"
export GPAW_SETUP_PATH=/zhome/86/d/1666/PAW/gpaw-setups-0.9.20000



# ASAP
#export PYTHONPATH=$COURSEHOME/asap/build/lib.linux-x86_64-3.6:$PYTHONPATH

#PyQSTEM
#export PYTHONPATH=$COURSEHOME/PyQSTEM/build/lib.linux-x86_64-3.6:$PYTHONPATH
